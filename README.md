TOMCAT plugins for Fiji
=======================

This is a collection of plugins and scripts to be used in Fiji for
TOMCAT-specific tasks.

Make sure to add your new script, plugin, or macro to the corresponding folder.

Documentation on individual plugins and macros:

  * [**Multiscale_Coords:**](docs/markdown/MultiscaleCoordsDocumentation.md) plugin to visually find the motor coordinates for high resolution ROI scans, taking as reference a low resolution overview scan.

  * [**Opt_Scan:**](docs/markdown/OptScanDocumentation.md) plugin to find the optimal high resolution scanning grid for irregular samples, taking as reference a low resolution overview scan.
