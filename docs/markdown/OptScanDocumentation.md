# Opt_Scan.ijm documentation

## Introduction & Purpose

Opt_Scan.ijm is an ImageJ plugin to find the optimal scanning grid for irregular objects. The plugin generates a .txt file that can be used as input for the beamline macro.

_Created by hector.dejea@psi.ch in February 2018_

## Instructions of use

*1.* Open your LowRes stack of images with ImageJ.

*2.* Initialise the plugin:
  * Locally: ImageJ -> Plugins -> Macros -> Run -> Browse script.
  * Beamline: ImageJ -> Plugins -> TOMCAT -> OptScan.

*3.* A dialog asking for LowRes and HighRes acquisition parameters will appear. Fill up with the corresponding inputs:
  * LowRes imagePixelSize: pixel size (um) in your LowRes scan.
  * LowRes XX center: XX motor position in your LowRes scan.
  * LowRes ZZ center: ZZ motor position in your LowRes scan.
  * LowRes Y position (B1): Y motor position of your highest (first) LowRes scan.
  * LowRes FOV Y: field of view in pixels in Y (vertical) of LowRes scan.
  * HighRes FOV X: field of view in pixels in X (horizontal) of HighRes scan.
  * HighRes FOV Y: field of view in pixels in Y (vertical) of HighRes scan.
  * HighRes imagePixelSize: pixel size (um) in your HighRes scan.
  * Camera movement X: due to the use of both end stations or the change of magnification, camera realignment might be needed. Introduce here the camera movement in X.
  * Camera movement Y: Following previous explanation, introduce here the camera movement in Y.
  * Overlap in X and Z (pixels): amount of overlapping HighRes pixels in the X and Z directions. Currently, it's not possible to have a different value for each direction.
  * Overlap in Y (pixels): amount of overlapping HighRes slices in the Y directions.
  * Projection Method: choose between maximum and minimum intensity projection depending if your object is brighter or darker than the background.

*NOTE 3.1:* For those interested in doing 360 deg HighRes scans, introduce the 360 deg FOV in _HighRes FOV X_.

<img src="images/OptScan/dialogue1.png"  width="250" height="350">


*4.* You will be asked to go to the first (top) and last (bottom) slice of the region that you want to include in your optimal scanning grid.

*5.* The number of Y layers will be computed and a maximum/minimum intensity projection will be generated for each of them and shown at a time. You should then place a rectangle covering your area of interest in each of the projections and the plugin will compute the necessary XX and ZZ coordinates to cover the selected areas.

*6.* The computed coordinates will be stored in a .txt file and saved in the desired path and with the desired name. Each line of the file will contain the XX, ZZ and Y coordinates separated by a coma of one of the volumes that need to be acquired. The volumes are ordered from left to right (ZZ), up to down (XX) and top to bottom (Y).

*7.* Finally, a dialog with a summary of the computation will pop up. If, for any reason, you wish to restart the process, check the box and press okay, which will bring you directly back to step 3.

*NOTE 7.1:* Both by restarting the plugin using the checkbox and byclosing and opening again, your last input values will appear as default in the new dialogs (see _'home/.optscan_settings.txt'_).

*NOTE 7.2:* In the beamline git repository: _beamline-scripts/scan/ForLargeSamples_ , you will find the _scanAtTXTCoords.py_ macro in order to launch scans with the coordinates saved in the txt file.