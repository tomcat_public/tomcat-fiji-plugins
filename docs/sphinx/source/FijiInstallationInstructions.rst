=======================================================
Fiji Installation Instructions including TOMCAT plugins
=======================================================

This page explains step by step the instructions for the installation of a new Fiji version from scratch with all relevant TOMCAT plugins.


1) Download and unpack the relevant Fiji.zip file

#) Copy the following files to the TOMCAT folder (to be created) in the existing Fiji plugins folder

   a. Reco_Manager.py
   #. DMP_Reader.class
   #. centering_overlay.py
   #. _multiscale_coords.txt
   #. ZeroMQ_Viewer-0.5.0.jar 

#) DMP viewer (`https://git.psi.ch/tomcat/TOMCAT-DMP-Files/tree/master/ImageJ`_)
      
   a. Copy *DMP_Reader.class* to the plugins/TOMCAT folder   
   #. Copy *HandleExtraFileTypes.class* to the plugins folder
   #. If necessary run
   
      i. cd location/of/Fiji/plugins    
      #. jar -uf IO\_-2.1.0.jar HandleExtraFileTypes.class
      
      
#) HDF5 Viewer (`https://github.com/paulscherrerinstitute/ch.psi.imagej.hdf5`_)

   a. Download and unzip plugin
   #. Decompile *HandleExtraFileTypes.class* to *HandleExtraFileTypes.java*
      (previously done with `Jad v1.5.8g`_, but not tried now)
            
   #. Add the following line to *HandleExtraFileTypes.java*:
   
      ::
         
         if (s1.endsWith(".h5") || s1.endsWith(".hdf5")) {
            return tryPlugIn("ch.psi.imagej.hdf5.HDF5Reader", s2);
         }
      
   #. Recompile using: javac -classpath ../jars/ij-1.51s.jar HandleExtraFileTypes.java   
   #. Run jar -uf IO\_-2.1.0.jar HandleExtraFileTypes.class
   
#) Jython
  
   a. Jython 2.7 has some problems:
   
      i. The *os* module is missing some attributes (e.g. getuid)
      #. *os.path.realpath* has a strange behavior
      
   #. Jython 2.5 should be used instead
   #. Therefore remove *jython-shaded-2.7.0.jar* from the Fiji jars folder
   #. Substitute it with *jython-shaded-2.5.3.jar* from an older Fiji distribution (if available) or from `here`_.    
    
    
#) Permissions
   
   a. Group for the personal account is different from the group of the e-account
   #. Therefore, others need to have read permissions for files and directories
   #. For this purpose, run /net/gfa-fs-3/export/tomcat/x86_64/bin/x02da_copy_permissions_fijiInstallation.py
   
   
   
.. _https\://git.psi.ch/tomcat/TOMCAT-DMP-Files/tree/master/ImageJ: https://git.psi.ch/tomcat/TOMCAT-DMP-Files/tree/master/ImageJ
.. _https\://github.com/paulscherrerinstitute/ch.psi.imagej.hdf5: https://github.com/paulscherrerinstitute/ch.psi.imagej.hdf5
.. _Jad v1.5.8g: http://www.javadecompilers.com/jad
.. _here: https://jar-download.com/explore-java-source-code.php?a=jython-shaded&g=org.scijava&v=2.5.3&downloadable=1