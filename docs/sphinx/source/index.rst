.. TOMCAT Fiji Plugins documentation master file, created by
   sphinx-quickstart on Thu Oct  5 13:45:28 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TOMCAT Fiji documentation!
=====================================

Contents:

Using FIJI at the beamline
--------------------------

.. toctree::
   :maxdepth: 1

   FijiInstallationInstructions.rst

TOMCAT plugins
--------------

.. toctree::
   :maxdepth: 1

   Multiscale_Coords.rst
   Opt_Scan.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

