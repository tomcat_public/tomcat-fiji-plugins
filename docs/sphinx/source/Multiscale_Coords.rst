.. Multiscale_Coords documentation master file, created by
   sphinx-quickstart on Thu Jun 29 14:02:29 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


**Multiscale_Coords.ijm documentation**
=======================================

**Introduction & Purpose**
--------------------------

Often, low resolution (LowRes) scans are acquired in order to have a global overview of the sample (usually fitting the field of view) and identify regions of interest (ROI). These ROIs are then scanned with higher resolution (HighRes) in order to observer finer structures (usually local tomography). To move the motors into the correct position for the HighRes scan requires tedious, not straightforward and error-prone calculations that need many parameters to be taken into account.

Multiscale_Coords.ijm is an ImageJ plugin to find the final/relative coordinates for moving the XX, ZZ and Y motors in a visual manner. The ROI size of the HighRes scan is shown according to the user-supplied parameters of LowRes and HighRes scans. The  user can move the ROI interactively to the desired position, and the movements in um are given as the result. Start and end position values for stitched scans are also available.

*Created by hector.dejea@psi.ch in May 2017 extending and adapting localtomo_coords.txt from bernd.pinzer@psi.ch (August 2011)*

*Reference paper: Dejea, H. et al. Scientific Reports 9, 6996 (2019)*

**Instructions of use**
-----------------------

*BEFORE STARTING, make sure that the beamline has been optimally prepared. Otherwise, shifts and errors in the motor positions are to be expected.*

1. Open your LowRes stack of images with ImageJ.

2. Initialise the plugin:

	* **Locally**: ImageJ -> Plugins -> Macros -> Run -> Browse script
	* **Beamline**: ImageJ -> Plugins -> TOMCAT -> Multiscale Coords

3. A dialog asking for LowRes and HighRes acquisition parameters will appear. Fill up with the corresponding inputs:
	- **LowRes imagePixelSize**: pixel size (um) in your LowRes scan.
	- **LowRes XX center**: XX motor position in your LowRes scan.
	- **LowRes ZZ center**: ZZ motor position in your LowRes scan.
	- **LowRes Y position**: Y motor position in your LowRes scan.
	- **HighRes FOV X**: field of view in pixels in X (horizontal) of HighRes scan.
	- **HighRes FOV Y**: field of view in pixels in Y (vertical) of HighRes scan.
	- **HighRes imagePixelSize**: pixel size (um) in your HighRes scan.
	- **Camera movement X**: due to the use of both end stations or the change of magnification, camera realignment might be needed. Introduce here the camera movement in X.
	- **Camera movement Y**: Following previous explanation, introduce here the camera movement in Y.
	- **Under development** Stage rotation (degrees): If the starting rotation angle is different from 0, introduce here.

.. note::
	* For those using a stitched scan as low resolution reference volume, introduce its Y center in *LowRes Y position_* (i.e. for most cases, take the average between the first and last Y positions of the volumes forming the stitched scan).

.. note::
	* For those interested in doing 360 deg HighRes scans, introduce the 360 deg FOV in *HighRes FOV X*.

4. A dialogue asking for stitched scan parameters will appear. Fill up the corresponding inputs:
	- **Overlap in X and Z (pixels)**: amount of overlapping pixels in the X and Z directions. The value shown by default is 1/3 of the input *HighRes FOV X*. Currently, it is not possible to have a different value for each direction.
	- **Overlap in Y (pixels)**: amount of overlapping slices in the Y directions.
	- **Scans in X**: size of the stitched scan grid in the X direction (the plugin assumes X to be vertical direction in reconstructed slices).
	- **Scans in Z**: size of the stitched scan grid in the Z direction (the plugin assumes Z to be horizontal direction in reconstructed slices).
	- **Number of Slices in Y (in LR)**: The number calculated by default gives you the amount of slices in LR corresponding to one volume in HR. If you reduce this value you will have one height done in HR, if you increase this value you will end up with a stacked scan. **Example:** I want to acquire in HR an area from LR slice 500 to 1500. Thus, *Number of Slices in Y (in LR) = 1000*.
	- **Selected Y slice as**: select the viewed slice to be the first (top) or the middle of the scan. The middle option is valid only for 1 Y block: no matter the value you introduce in *Slices in Y*, a safety "IF" will put it back to default.

.. note::
	* Set Scans in X *AND* Scans in Z to 1 in order to have one single scan in the XX/ZZ plane.

.. note:: 
	* If *LowRes ZZ center, LowRes XX center, LowRes Y position,* and *Camera movements* are left to 0, only the relative movements will be given.
	* If the value given to *Slices in Y* is smaller than the FOV Y in HighRes, the total FOV will be considered.

Once OK has been clicked, a circle (single scan) or a rectangle (stitched scan) with the size of the area to be scanned given your inputs will appear in the image.

.. image:: images/MultiscaleCoords/dialogue1.png
   :height: 8cm
.. image:: images/MultiscaleCoords/dialogue2.png
   :height: 8cm


5. Move the ROI to the position in which you would like to perform the high resolution scans and press OK. If you selected stitched scan, the circles corresponding to each of the individual scans will be shown.

.. note::
	In case the size of the ROI shown is not adequate for your purposes, press Shift+OK and the stitched scan parameters dialog will pop up again.

.. image:: images/MultiscaleCoords/single.png
   :height: 8cm
.. image:: images/MultiscaleCoords/stitch.png
   :height: 8cm
.. image:: images/MultiscaleCoords/stitchfin.png
   :height: 8cm

6. Finally, a dialog with the relative motor movements and absolute motor position values will pop up. In addition, a command line with the corresponding inputs will be generated. If, for any reason, you wish to restart the process, check the box and press okay, which will bring you directly back to step 3. If you would like to save the motor positions or Fiji coordinates of the ROI for all volumes in txt files, check the corresponding boxes.

.. note::
	* Both by restarting the plugin using the checkbox and byclosing and opening again, your last input values will appear as default in the new dialogs (see '*<home>/.multiscale_settings.txt'*).

.. note::
	* Before launching the high resolution scan, make sure that the given motor positions lead to the amount of scans desired. Sometimes, tiny calculation errors will lead to values that make the system scan 1 extra volume in a specific direction. You can check with "-t" in the macro and, if this is the case, change one of the motor positions by 0.1 to make sure you have the correct amount of volumes.

.. note::
	* In the beamline git repository: *beamline-scripts/scan/ForLargeSamples* , you will find the *multiplescan.sh* macro in order to launch several stitching macros at the same time.

.. image:: images/MultiscaleCoords/finalDialog.png
   :height: 9cm
   :align: center

7. If you checked the motor positions txt file box, a new dialog will appear. This dialog asks whether you want to save the volume coordinates in the conventional order (ZZ, XX, Y) or in the optimised order (Y, ZZ, XX). The optimised order will result in faster acquisitions, since the Y motor is the fastest. Once you press OK, you will be allowed to browse to the target directory and to restart the plugin.

.. image:: images/MultiscaleCoords/saveTXToptions.png
   :height: 3cm
   :align: center

.. note::
	* In the beamline git repository: *beamline-scripts/scan/ForLargeSamples* , you will find the *scanAtTXTCoords.py* macro in order to launch scans with the coordinates saved in the txt file.





.. toctree::
   :maxdepth: 2
   :caption: Contents:
