// draw a crosshair in the center of the image
h = getHeight();
w = getWidth();
setColor("yellow");
Overlay.remove;
Overlay.drawLine(w/2.0, 0, w/2.0, h);
Overlay.add;
Overlay.drawLine(0, h/2.0, w, h/2.0);
Overlay.add;
Overlay.show;
